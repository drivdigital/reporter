<?php

/**
 * Plugin Name: Driv Report Receiver
 * Plugin URI: http://www.drivdigital.no
 * Description: N/A.
 * Version: ##VERSION##
 * Author: Driv Digital
 * Author URI: http://www.drivdigital.no
 * License: MIT
 */
class Driv_Report_Receiver {

  private $post;

  public function __construct() {
    add_action( 'init', array( $this, 'process_post_request' ) );
    add_filter( 'comment_text', array( $this, 'parse_post_comment' ), 10, 2 );
  }

  /**
   * Process post requests. If the request is the Driv Report Sender, try to find
   * the web site post and set the meta data.
   */
  public function process_post_request() {

    if ( isset( $_POST['cms'] ) ) {
      // Get the host name component from the url as website posts uses the host
      // name as post title.
      $host_name = $this->get_host_name_from_url( $_POST['site-url'] );
      $post      = $this->find_website_post_by_url( $host_name );

      if ( $post == null ) {
        $post = $this->new_post( $_POST['site-url'] );
      }
      else {
        $this->diff_data( $post->ID );
      }

      // Create diff info.
      $this->set_meta( $post->ID, '_report_cms_name', $_POST['cms'] );
      $this->set_meta( $post->ID, '_report_cms_version', $_POST['version'] );
      $this->set_meta( $post->ID, '_report_plugins', $_POST['plugins'] );
      $this->set_meta( $post->ID, '_report_mu_plugins', $_POST['mu-plugins'] );
      $this->set_meta( $post->ID, '_report_last_updated', time() );

      die( 'ok' );
    }
  }

  function new_post( $url ) {
    $post = array(
        'post_name'      => $this->get_host_name_from_url( $url ),
        'post_title'     => $url,
        'post_status'    => 'publish',
        'post_type'      => 'website',
        'comment_status' => 'open'
    );
    return get_post( wp_insert_post( $post ) );
  }

  function diff_data( $post_id ) {
    $new_info      = $this->create_data_for_diff(
        $_POST['cms'],
        $_POST['version'],
        $_POST['plugins'],
        $_POST['mu-plugins']
    );
    $existing_info = $this->create_data_for_diff(
        get_post_meta( $post_id, '_report_cms_name', true ),
        get_post_meta( $post_id, '_report_cms_version', true ),
        get_post_meta( $post_id, '_report_plugins', true ),
        get_post_meta( $post_id, '_report_mu_plugins', true ) );
    $diff_data     = array_diff_assoc( $existing_info, $new_info );

    if ( ! empty( $diff_data ) ) {
      file_put_contents( '/logs/reporter-plugin.log', var_export( $diff_data, 1 ) );
      $comment = array(
          'comment_post_ID'  => $post_id,
          'comment_content'  => var_export( $diff_data, 1 ),
          'comment_approved' => 1,
      );
      wp_insert_comment( $comment );
    }
  }

  function create_data_for_diff( $cms_name, $cms_version, $plugins, $mu_plugins ) {
    $data = array(
        'cms_name'    => $cms_name,
        'cms_version' => $cms_version,
    );

    foreach ( $plugins as $plugin => $plugin_info ) {
      $active                                 = $plugin_info['active'] == 1 ? 'activated' : 'deactivated';
      $data['plugin_' . $plugin_info['name']] = $active . ' - ' . $plugin_info['version'];
    }

    foreach ( $mu_plugins as $mu_plugin => $plugin_info ) {
      $active                                    = $plugin_info['active'] == 1 ? 'activated' : 'deactivated';
      $data['mu_plugin_' . $plugin_info['name']] = $active . ' - ' . $plugin_info['version'];
    }

    return $data;
  }

  /**
   * Tries to find the website post by url.
   * fixme: Performance, check if it is possible to filter the result using
   * query only.
   *
   * @param String $url_to_find
   * @return mixed a WP_Post or null if not found.
   */
  function find_website_post_by_url( $url_to_find ) {
    $args  = array(
        'post_type'   => 'website',
        'post_status' => 'publish',
        'numberposts' => 1,
        'meta_query'  => array(
            array(
                'key'   => 'wpcf-url',
                'value' => $url_to_find,
            )
        )
    );
    $posts = get_posts( $args );
    if ( $posts ) {
      return $posts[0];
    }
    return null;
  }

  /**
   * Sets post meta data for the post id. If the key does not exist, it is created.
   *
   * @param Integer $post_id
   * @param String $key
   * @param String $value
   */
  function set_meta( $post_id, $key, $value ) {
    if ( get_post_meta( $post_id, $key ) ) {
      update_post_meta( $post_id, $key, $value );
    }
    else {
      add_post_meta( $post_id, $key, $value );
    }
  }

  /**
   * Returns the host name of the URL.
   *
   * @param String $url
   * @return String
   */
  function get_host_name_from_url( $url ) {
    $url_parts = parse_url( $url );
    return $url_parts['host'] ? $url_parts['host'] : $url;
  }

  public function parse_post_comment( $comment_content, $comment ) {
    if ( ! $this->post ) {
      $this->post = get_post( $comment->comment_post_ID );
    }

    // Only run this for website post.
    if ( $this->post->post_type != 'website' ) {
      return $comment;
    }

    $content = trim( $comment_content );

    // Todo: robustify.
    if ( substr( $content, 0, 5 ) != 'array' ) {
      return $content;
    }
    $content = str_replace( PHP_EOL, '', $content );
    $content = preg_match( '/\(([^]]+)\)/', $content, $matches );
    $content = $matches[1];
    $content = preg_replace( '/&#8216;/', '', $content );
    $content = preg_replace( '/&#8217;/', '', $content );
    $content = preg_replace( '/&#8242;/', '', $content );
    $content = preg_replace( '/&#8211;/', '-', $content );
    $lines   = explode( ',', $content );
    array_pop( $lines );

    $content = '';
    foreach ( $lines as $line ) {
      $columns = explode( ' => ', $line );
      $key     = str_replace( 'cms_version', 'CMS Version', $columns[0] );
      $key     = str_replace( 'mu_plugin_', 'mu plugin - ', $key );
      $key     = str_replace( 'plugin_', 'plugin - ', $key );
      $value   = $columns[1];
      $content .= $key . ' - ' . $value . PHP_EOL;
    }
    return $content;
  }

}

new Driv_Report_Receiver();
