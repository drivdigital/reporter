require( 'shelljs/global' );

if ( ! which( 'git' ) ) {
    echo( 'Sorry, this script requires git' );
    exit( 1 );
}

var release_dir = 'release';
var change_log_file = 'changelog.md';
var version_info = get_version_info();
var version = version_info.version;

if ( ! version_info ) {
    echo( 'Invalid version info. Should start with Version n.n (YYYY-mm-dd). Check ' + change_log_file + '\n' );
    exit( 1 );
}
var release_date = version_info.release_date;
var source_files = ['report-receiver.php', 'report-sender.php', change_log_file];

echo( 'Creating release...\n' );

/**
 * 1. Clean and create release dir.
 * 2. Copy source files to release dir.
 * 3. Replace occurrences of ##VERSION## with version number
 */
rm( '-rf', release_dir );
mkdir( '-p', release_dir );
cp( source_files, release_dir );
cd( release_dir );
ls( '*.php' ).forEach( function ( file ) {
    sed( '-i', '##VERSION##', version, file );
} );

/**
 * Create zip.
 */
var zip_file_name = 'reporter-dist-' + version + '_' + release_date.replace( /-/g, '' ) + '.zip';
exec( 'zip ' + zip_file_name + ' ' + source_files.join( ' ' ), {silent: true} );
cd( '..' );

exec( 'echo "Version x.x (xxxx-xx-xx)" | cat - ' + change_log_file + ' > /tmp/tempfile && mv /tmp/tempfile ' + change_log_file );

/**
 * Done.
 */
echo( 'Done.\nRemember to upload the file (' + release_dir + '/' + zip_file_name + ') to the repo: ' + get_remote_upload_url() + ' \n' );

function get_version_info() {
    try {
        var content = cat( change_log_file );
        // Version 0.4 (2015-10-21)
        var first_line = content.match( /^(.*)$/m )[0];
        var parts = first_line.match( /Version (\d\.\d) \((\d{4}-\d{2}-\d{2})\)/ );
        return {
            version:      parts[1],
            release_date: parts[2]
        };
    }
    catch ( ex ) {
        return false;
    }
}


function get_remote_upload_url() {
    var result = 'https://bitbucket.org';
    var remote = exec( 'git config --get remote.origin.url', {silent: true} ).output;
    remote = remote.split( ':' );
    var path = remote[1].split( '.' )[0];

    result += '/' + path + '/downloads';

    return result;
}