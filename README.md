This Word Press plugin posts installation information to your services at a daily interval.

## Features

* Posts the following information: WP version, plugins info (name, version, active)
* Configure service URL's to post the information to.
* Sends the information at a daily interval. (uses WP Cron).
* Trigger posting manually.

## Install

1. Download zip the zip file found in the download section.
2. Unzip.
3. Upload the sender.php file in you customers mu-plugin directory.
4. Configure the options from the settings/Reporter page.
5. Upload the receiver.php file in yours mu-plugin directory.
4. Navigate to the plugin management page in WP and enable the plugin.

## Hacking

Before deploying the plugins you should create a release. 
This will create a zip file in the release/ directory ready for deployment.

Creating a release:

* \> npm install shelljs --save-dev
* \> node release

## Issues

Report bugs, issues and feature requests in Issues.
