Version x.x (xxxx-xx-xx)
Version 0.6 (2016-01-22)
* Fixed build script

Version 0.5 (2015-10-29)
* Fixed build script

Version 0.4 (2015-10-21)
* Introduced release script

Version 0.3 (2015-10-21)
* PD-61: Use website URL-field as key
* Improved formatting of info in website comments.
