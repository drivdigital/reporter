<?php

/**
 * Plugin Name: Report Sender
 * Plugin URI: http://www.drivdigital.no
 * Description: Posts information about the WP setup to a comma separated list of URLs. See WP Admin -> Settings -> Report
 * Version: ##VERSION##
 * Author: Driv Digital
 * Author URI: http://www.drivdigital.no
 * License: MIT
 */

class Report_Sender {
  const PAGE_TITLE      = 'Reporter Admin';
  const MENU_TITLE      = 'Reporter';
  const MENU_CAPABILITY = 'manage_options';
  const MENU_SLUG       = 'reporter-sender-admin';
  const OPTION_GROUP    = 'reporter_sender_options';
  const OPTION_NAME     = 'reporter';

  /**
   * Plugin options.
   * @var mixed $options
   */
  private $options;

  /**
   * Constructor.
   */
  public function __construct() {
    // Register plugin activation hooks for the scheduled event.
    register_activation_hook( __FILE__, array( $this, 'on_activate_plugin' ) );
    register_deactivation_hook( __FILE__, array( $this, 'on_deactivate_plugin' ) );

    add_action( 'reporter_send_info', array( $this, 'send_data' ) );
    add_action( 'admin_menu', array( $this, 'add_menu_item' ) );
    add_action( 'admin_init', array( $this, 'page_init' ) );

    if ( isset( $_POST['reporter_manually_send_info'] ) && $_POST['reporter_manually_send_info'] == 'true' ) {
      $this->send_data();
    }
  }

  /**
   * Called when the plugin is activated.
   *
   * @see: register_activation_hook
   */
  public function on_activate_plugin() {
    // Send the information on a daily basis.
    wp_schedule_event( time(), 'daily', 'reporter_send_info' );
  }

  /**
   * Called when the plugin is deactivated.
   *
   * @see: register_deactivation_hook
   */
  public function on_deactivate_plugin() {
    // Make sure the scheduled hook is cleared on plugin deactivation.
    wp_clear_scheduled_hook( 'reporter_send_info' );
  }

  /**
   * Adds the menu item to Word Press' Settings menu.
   */
  public function add_menu_item() {
    add_options_page(
        self::PAGE_TITLE,
        self::MENU_TITLE,
        self::MENU_CAPABILITY,
        self::MENU_SLUG,
        array( $this, 'create_admin_page' )
    );
  }

  /**
   * Creates the plug-in's admin settings page.
   */
  public function create_admin_page() {
    $this->options = get_option( self::OPTION_NAME );
    ?>
    <div class="wrap">
      <form method="post" action="options.php">
        <?php
        settings_fields( self::OPTION_GROUP );
        do_settings_sections( self::MENU_SLUG );
        ?>
        <div>
          <input type="hidden" name="reporter_manually_send_info" value="false">
          <input type="submit"
                 id="reporter-send-info-button"
                 value="Save and Send Data"
                 class="button"
                 title="Manually send data"/>
        </div>
        <?php submit_button(); ?>
      </form>
      <script type="text/javascript">
        // Adds an event listener and callback to the manually send info button.
        jQuery('#reporter-send-info-button').click(function () {
          jQuery('input[name=reporter_manually_send_info]').val('true');
        });
      </script>
    </div>
  <?php
  }

  /**
   * Set's up the options using Word Press' options API.
   * Called when the user accesses the admin area.
   */
  public function page_init() {
    // Register a setting and its sanitization callback.
    register_setting(
        self::OPTION_GROUP,
        self::OPTION_NAME,
        array( $this, 'sanitize' )
    );

    // Set up the form.
    $general_section_id = 'general_settings';
    add_settings_section(
        $general_section_id,
        self::MENU_TITLE,
        array( $this, 'print_form_section_info' ),
        self::MENU_SLUG // Page
    );

    // Adds the input fields to the section.
    add_settings_field(
        'urls',
        'URLs:',
        array( $this, 'urls_textarea_element_callback' ),
        self::MENU_SLUG,
        $general_section_id
    );
  }

  /**
   * Creates the form section heading.
   */
  public function print_form_section_info() {
    print 'Enter your settings below:';
  }

  /**
   * Sanitizes the form data before it is stored.
   *
   * @param array $form_data
   * @return mixed
   */
  public function sanitize( $form_data ) {
    $new_form_data = $form_data;

    if ( isset( $form_data['urls'] ) ) {
      $new_form_data['urls'] = sanitize_text_field( $form_data['urls'] );
    }
    return $new_form_data;
  }

  /**
   * Creates the textarea element.
   *
   * See: page_init
   */
  public function urls_textarea_element_callback() {
    printf(
        '<textarea id="urls" name="%s" style="width: 100%%; height: 200px">%s</textarea>',
        $this->create_input_name( "urls" ),
        isset( $this->options['urls'] ) ? esc_attr( $this->options['urls'] ) : ''
    );
  }

  /**
   * Posts the data to found  URL's in the option URLs field.
   */
  public function send_data() {
    // Get the comma separated list of URLs from the options.
    $urls = get_option( self::OPTION_NAME )['urls'];
    if ( !isset( $urls ) && $urls != '' ) {
      die( 'Could not send. No urls configured.' );
    }

    $post_arguments = array(
        'body'    => $this->create_post_data(),
        'timeout' => 10
    );

    // Split the comma separated urls and http post the installation info.
    $urls = explode( ',', $urls );
    foreach ( $urls as $url ) {
      wp_remote_post( trim( $url ), $post_arguments );
    }
  }

  /**
   * Creates an array with information (WP-version, plugin info etc.) about the WP installation.
   *
   * @return array
   */
  private function create_post_data() {
    if ( !function_exists( 'get_plugins' ) ) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $data = array(
        'cms'        => 'WordPress',
        'site-url'   => get_site_url(), // fixme: this will fetch the url of the default blog/site. Should we send a blog_id param?
        'version'    => get_bloginfo( 'version' ),
        'plugins'    => $this->create_plugin_data( get_plugins(), false ),
        'mu-plugins' => $this->create_plugin_data( get_mu_plugins(), true ),
    );
    return $data;
  }

  /**
   * Returns an array with sanitized information about the installations plugins.
   *
   * @param array $plugins An array with word press plugins.
   * @param boolean $mu true if the array contains mu plugins.
   * @return array
   */
  private function create_plugin_data( $plugins, $mu = false ) {
    $data = array();
    foreach ( $plugins as $key => $value ) {
      if ( $mu ) {
        $dir_name = $key;
      } else {
        $dir_name = explode('/',$key)[0];
      }
      $data[$key] = array(
          'name'     => $plugins[$key]['Name'],
          'dir_name' => $dir_name,
          'version'  => $plugins[$key]['Version'],
          'active'   => is_plugin_active( $key )
      );
    }
    return $data;
  }

  /**
   * Helper. Creates a valid input name for a form input field.
   * Needed for the WP settings API.
   *
   * @param string $name
   * @return string A valid WP settings input field name.
   */
  private function create_input_name( $name ) {
    return self::OPTION_NAME . '[' . $name . ']';
  }

}

new Report_Sender();
